Website
=======
https://www.libsdl.org/

License
=======
zlib license (see the file source/COPYING.txt)

Version
=======
2.0.9

Source
======
SDL2-2.0.9.tar.gz (sha256: 255186dc676ecd0c1dbf10ec8a2cc5d6869b5079d8a38194c2aecdff54b324b1)

Required by
===========
* SDL2_image
* SDL2_mixer
* SDL2_ttf
* smpeg
